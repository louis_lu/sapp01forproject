//
//  Store.m
//  Dingbendong
//
//  Created by mac on 13/11/21.
//  Copyright (c) 2013年 louis.lu. All rights reserved.
//

#import "Store.h"

@implementation Store
@synthesize name;
@synthesize supplierID;
@synthesize address;
@synthesize phoneNumber;

-(id)initWithCoder:(NSCoder *)aDecoder{
    //此時super是NSObject
    if(self=[super init]){
        self.name=[aDecoder decodeObjectForKey:@"name"];
        self.supplierID=[aDecoder decodeObjectForKey:@"supplierID"];
        self.address=[aDecoder decodeObjectForKey:@"address"];
        self.phoneNumber=[aDecoder decodeObjectForKey:@"phoneNumber"];
        
        
    }
    return self;
}
-(void)encodeWithCoder:(NSCoder *)aCoder{
    
    
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.supplierID forKey:@"supplierID"];
    [aCoder encodeObject:self.address forKey:@"address"];
    [aCoder encodeObject:self.phoneNumber forKey:@"phoneNumber"];

}
 
@end
