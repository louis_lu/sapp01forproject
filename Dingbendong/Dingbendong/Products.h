//
//  Products.h
//  Dingbendong
//
//  Created by mac on 13/11/27.
//  Copyright (c) 2013年 louis.lu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Products : NSObject
{
    NSString * productName;
    NSString * productID;
    NSString * unitPrice;
    NSString * supplierID;
}
@property(nonatomic, retain) NSString * productName;
@property(nonatomic, retain) NSString * productID;
@property(nonatomic, retain) NSString * unitPrice;
@property(nonatomic, retain) NSString * supplierID;
@end
