//
//  DBViewController.m
//  Dingbendong
//
//  Created by mac on 13/11/19.
//  Copyright (c) 2013年 louis.lu. All rights reserved.
//

#import "DBViewController.h"

#import "DBMenuViewController.h"

//#define localhost lulouis.no-ip.biz
@interface DBViewController ()<DBMenuViewControlleDelegate>

@end

@implementation DBViewController



-(id)initWithCoder:(NSCoder *)aDecoder{
    
    if (self=[super initWithCoder:aDecoder]){
        stores =[NSMutableArray array];

        
        
        
        //對select_from_suppliers.php提出請求
        NSString *  storeWebsite ;
        storeWebsite =[[NSString alloc]initWithFormat:@"http://localhost:8888/sapp01/select_from_suppliers.php"];
        storeWebsite = [storeWebsite stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURLRequest *storeWebsiterequest  =[NSURLRequest requestWithURL:[NSURL URLWithString:storeWebsite]];
        
        NSData * storeWebsiteData =[NSURLConnection sendSynchronousRequest:storeWebsiterequest returningResponse:nil error:nil];
        
        NSDictionary * theJSsonDict_storeWebsite =
        [NSJSONSerialization JSONObjectWithData:storeWebsiteData options:NSJSONReadingAllowFragments error:nil];
        /*
        NSString * strTempstorelist;
        for( int i = 0 ; i < [theJSsonDict_storeWebsite[@"array_of_rows"] count]; i++ )
        {
            strTempstorelist = [NSString stringWithFormat:@"%@\n", theJSsonDict_storeWebsite[@"array_of_rows"][i][@"company_name"] ];
            NSLog(@"index%d %@",i, theJSsonDict_storeWebsite[@"array_of_rows"][i][@"company_name"]);
        }
        */
        
        //對select_from_products.php發出請求
        NSString *  storeWebsiteForProducts ;
        storeWebsiteForProducts =[[NSString alloc]initWithFormat:@"http://localhost:8888/sapp01/select_from_products.php"];
        
        storeWebsiteForProducts = [storeWebsiteForProducts stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURLRequest *storeWebsiteForProductsrequest  =[NSURLRequest requestWithURL:[NSURL URLWithString:storeWebsiteForProducts]];
        
        NSData * storeWebsiteDataForProducts =[NSURLConnection sendSynchronousRequest:storeWebsiteForProductsrequest returningResponse:nil error:nil];
        
        NSDictionary * theJSsonDict_storeWebsiteForProducts =
        [NSJSONSerialization JSONObjectWithData:storeWebsiteDataForProducts options:NSJSONReadingAllowFragments error:nil];
        
         NSString * strTempstorelist;
         for( int i = 0 ; i < [theJSsonDict_storeWebsiteForProducts[@"array_of_rows"] count]; i++ )
         {
         strTempstorelist = [NSString stringWithFormat:@"%@\n", theJSsonDict_storeWebsiteForProducts[@"array_of_rows"][i][@"product_id"] ];
         NSLog(@"index%d %@",i, theJSsonDict_storeWebsiteForProducts[@"array_of_rows"][i][@"product_id"]);
         }
         
        //Store * store =[[Store alloc]init];
        
        for(int i=0;i<[theJSsonDict_storeWebsite[@"array_of_rows"] count];i++){
            
            
            Store * store =[[Store alloc]init];

            
            
            NSString * content =[NSString stringWithFormat:theJSsonDict_storeWebsite[@"array_of_rows"][i][@"company_name"],i];
            NSString * sID=[NSString stringWithFormat:theJSsonDict_storeWebsite[@"array_of_rows"][i][@"supplier_id"],i];
            NSString * sAddress=[NSString stringWithFormat:theJSsonDict_storeWebsite[@"array_of_rows"][i][@"address"],i];
            NSString * sMobilePhones=[NSString stringWithFormat:theJSsonDict_storeWebsite[@"array_of_rows"][i][@"phone_number"],i];
            
            
            store.name =content;
            store.supplierID=sID;
            store.address=sAddress;
            store.phoneNumber=sMobilePhones;
            
            [stores addObject:store];

            

            NSLog(@"%d",stores.count);

        }
        
/*
        for(int i=0;i<[theJSsonDict_storeWebsiteForProducts[@"array_of_rows"] count];i++){
        Store * store =[[Store alloc]init];

        NSString * pID =[NSString stringWithFormat:theJSsonDict_storeWebsite[@"array_of_rows"][i][@"product_id"],i];
        NSString * PName =[NSString stringWithFormat:theJSsonDict_storeWebsite[@"array_of_rows"][i][@"product_name"],i];
        NSString * uPrice =[NSString stringWithFormat:theJSsonDict_storeWebsite[@"array_of_rows"][i][@"unit_price"],i];
            
            store.productID=pID;
            store.productName=PName;
            store.unitPrice=uPrice;
            [stores addObject:store];

        }
*/
      

    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.storeTableView.delegate=self;
    self.storeTableView.dataSource=self;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

//    NSIndexPath* select = [self.storeTableView indexPathForSelectedRow];
//    int i =  select.row;
//    Store* store = [stores objectAtIndex:i];
//    NSString* strName = store.name;
//    strName =@"";
    
    if ( [segue.identifier isEqualToString:@"push"] ){

        NSLog(@"testmodal");
       
        
        DBMenuViewController *DBmenuViewController = segue.destinationViewController;
        
        NSIndexPath *indexPath = [self.storeTableView indexPathForSelectedRow];
        
        Store * store=stores[indexPath.row];
        
        DBmenuViewController.delegate = self;
        
       DBmenuViewController.store=store;
        
        
        
        
        
        
        
    }
    
}

-(IBAction)back:(UIStoryboardSegue *)segue{

}

- (IBAction)refreshButtonPressed:(id)sender {

    //louis add
            [stores removeAllObjects];
//    for( int i = 0 ; i < storeNames.count ; i++)
//    {
//
//        NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:1 inSection:0];
//                NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow:2 inSection:0];
//                NSIndexPath *indexPath3 = [NSIndexPath indexPathForRow:3 inSection:0];
//                        NSIndexPath *indexPath4 = [NSIndexPath indexPathForRow:4 inSection:0];
//                        NSIndexPath *indexPath5 = [NSIndexPath indexPathForRow:5 inSection:0];
//        [self.storeTableView deleteRowsAtIndexPaths:@[indexPath1,indexPath2, indexPath3, indexPath4, indexPath5] withRowAnimation:UITableViewRowAnimationNone];
//        
//
//    }
    

//
//    Store *storename1 =[[Store alloc]init];
//        
//        storename1.name =@"FIRST";
//        [storeNames addObject:storename1];
//    
//    [storeNames addObject:storename1];
//    
//    [storeNames addObject:storename1];
//    [storeNames addObject:storename1];
//    [storeNames addObject:storename1];
//
//    
//    NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow:storeNames.count inSection:0];
//
//    [self.storeTableView insertRowsAtIndexPaths:@[indexPath2] withRowAnimation:UITableViewRowAnimationNone];
//    return;

    
    
    
    
    NSString *  storeWebsite ;
    storeWebsite =[[NSString alloc]initWithFormat:@"http://localhost:8888/sapp01/select_from_suppliers.php"];
    storeWebsite = [storeWebsite stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURLRequest *storeWebsiterequest  =[NSURLRequest requestWithURL:[NSURL URLWithString:storeWebsite]];
    
    NSData * storeWebsiteData =[NSURLConnection sendSynchronousRequest:storeWebsiterequest returningResponse:nil error:nil];
    
    NSDictionary * theJSsonDict_storeWebsite =
    [NSJSONSerialization JSONObjectWithData:storeWebsiteData options:NSJSONReadingAllowFragments error:nil];
    /*
    NSString * strTempstorelist;
    for( int i = 0 ; i < [theJSsonDict_storeWebsite[@"array_of_rows"] count]; i++ )
    {
        strTempstorelist = [NSString stringWithFormat:@"%@\n", theJSsonDict_storeWebsite[@"array_of_rows"][i][@"company_name"] ];
        NSLog(@"index%d %@",i, theJSsonDict_storeWebsite[@"array_of_rows"][i][@"company_name"]);
    }
    */
    
    
    for(int i=0;i<[theJSsonDict_storeWebsite[@"array_of_rows"] count];i++){
        
        Store *store =[[Store alloc]init];
        
        NSString * content =[NSString stringWithFormat:theJSsonDict_storeWebsite[@"array_of_rows"][i][@"company_name"],i];
        NSString * sID=[NSString stringWithFormat:theJSsonDict_storeWebsite[@"array_of_rows"][i][@"supplier_id"],i];
        NSString * sAddress=[NSString stringWithFormat:theJSsonDict_storeWebsite[@"array_of_rows"][i][@"address"],i];
        NSString * sMobilePhones=[NSString stringWithFormat:theJSsonDict_storeWebsite[@"array_of_rows"][i][@"phone_number"],i];
        
        
        store.name =content;
        store.supplierID=sID;
        store.address=sAddress;
        store.phoneNumber=sMobilePhones;
        
        [stores addObject:store];


        NSLog(@"%d",stores.count);

        
    }
 
//     NSString *firstStoreName =theJSsonDict_storeWebsite[@"array_of_rows"][0][@"company_name"];
//     NSString *secondStoreName =theJSsonDict_storeWebsite[@"array_of_rows"][1][@"company_name"];
//     NSString *thirdStoreName =theJSsonDict_storeWebsite[@"array_of_rows"][2][@"company_name"];
//     
//     storeNames=[NSMutableArray arrayWithObjects:firstStoreName,secondStoreName,thirdStoreName, nil];
 
    /*
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:storeNames.count inSection:0];
    

    [self.storeTableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
     */
    [self.storeTableView reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark UITableView protocol
//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    m_nCurrentSelect = (int)indexPath.row;
//}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return stores.count ;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Store * store=stores[indexPath.row];
    //Store * store=productsID[indexPath.row];
    
    //storyboard,ios6
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    

    cell.textLabel.text = store.name;
    //cell.textLabel.text=store.productID;
    
    
    
    return cell;
}





//#pragma mark UISearchBarDelegate
//- (void)searchBarCancelButtonClicked{
//   
//    if([self.searchBar isFirstResponder]){
//        [self.searchBar resignFirstResponder];
//    }
//
//}

@end
