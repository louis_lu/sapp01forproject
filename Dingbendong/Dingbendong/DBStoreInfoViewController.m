//
//  DBStoreInfoViewController.m
//  Dingbendong
//
//  Created by louis on 13/11/23.
//  Copyright (c) 2013年 louis.lu. All rights reserved.
//

#import "DBStoreInfoViewController.h"

//#define localhost lulouis.no-ip.biz

@interface DBStoreInfoViewController ()

@end

@implementation DBStoreInfoViewController

-(id)initWithCoder:(NSCoder *)aDecoder{
    
    
    if (self=[super initWithCoder:aDecoder]){
        stores =[NSMutableArray array];
        
        NSString *  storeWebsite ;
        storeWebsite =[[NSString alloc]initWithFormat:@"http://localhost:8888/sapp01/select_from_suppliers.php"];
        storeWebsite = [storeWebsite stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURLRequest *storeWebsiterequest  =[NSURLRequest requestWithURL:[NSURL URLWithString:storeWebsite]];
        
        NSData * storeWebsiteData =[NSURLConnection sendSynchronousRequest:storeWebsiterequest returningResponse:nil error:nil];
        
        NSDictionary * theJSsonDict_storeWebsite =
        [NSJSONSerialization JSONObjectWithData:storeWebsiteData options:NSJSONReadingAllowFragments error:nil];
        NSString * strTempstorelist;
        for( int i = 0 ; i < [theJSsonDict_storeWebsite[@"array_of_rows"] count]; i++ )
        {
            strTempstorelist = [NSString stringWithFormat:@"%@\n", theJSsonDict_storeWebsite[@"array_of_rows"][i][@"company_name"] ];
            NSLog(@"index%d %@",i, theJSsonDict_storeWebsite[@"array_of_rows"][i][@"company_name"]);
        }
        
        
        
        for(int i=0;i<[theJSsonDict_storeWebsite[@"array_of_rows"] count];i++){
            
            Store *store =[[Store alloc]init];
            
            NSString *content =[NSString stringWithFormat:theJSsonDict_storeWebsite[@"array_of_rows"][i][@"company_name"],i];
            
            store.name =content;
            [stores addObject:store];
            NSLog(@"%d",stores.count);
            
        }
        
        
        //        NSString *firstStoreName =theJSsonDict_storeWebsite[@"array_of_rows"][0][@"company_name"];
        //        NSString *secondStoreName =theJSsonDict_storeWebsite[@"array_of_rows"][1][@"company_name"];
        //        NSString *thirdStoreName =theJSsonDict_storeWebsite[@"array_of_rows"][2][@"company_name"];
        //
        //        storeNames=[NSMutableArray arrayWithObjects:firstStoreName,secondStoreName,thirdStoreName, nil];
        
    }
    
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.storeTableView.delegate=self;
    self.storeTableView.dataSource=self;
	// Do any additional setup after loading the view.
}



- (IBAction)refreshButtonPressed:(id)sender {
     [stores removeAllObjects];
    NSString *  storeWebsite ;
    storeWebsite =[[NSString alloc]initWithFormat:@"http://localhost:8888/sapp01/select_from_suppliers.php"];
    storeWebsite = [storeWebsite stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURLRequest *storeWebsiterequest  =[NSURLRequest requestWithURL:[NSURL URLWithString:storeWebsite]];
    
    NSData * storeWebsiteData =[NSURLConnection sendSynchronousRequest:storeWebsiterequest returningResponse:nil error:nil];
    
    NSDictionary * theJSsonDict_storeWebsite =
    [NSJSONSerialization JSONObjectWithData:storeWebsiteData options:NSJSONReadingAllowFragments error:nil];
    NSString * strTempstorelist;
    for( int i = 0 ; i < [theJSsonDict_storeWebsite[@"array_of_rows"] count]; i++ )
    {
        strTempstorelist = [NSString stringWithFormat:@"%@\n", theJSsonDict_storeWebsite[@"array_of_rows"][i][@"company_name"] ];
        NSLog(@"index%d %@",i, theJSsonDict_storeWebsite[@"array_of_rows"][i][@"company_name"]);
    }
    
    
    
    for(int i=0;i<[theJSsonDict_storeWebsite[@"array_of_rows"] count];i++){
        
        Store *store =[[Store alloc]init];
        
        NSString *content =[NSString stringWithFormat:theJSsonDict_storeWebsite[@"array_of_rows"][i][@"company_name"],i];
        
        store.name =content;
        [stores addObject:store];
        NSLog(@"%d",stores.count);
        
    }

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark UITableView protocol
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return stores.count ;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Store * store=stores[indexPath.row];
    
    
    //storyboard,ios6
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    
    cell.textLabel.text = store.name;
    
    
    
    
    return cell;
}


@end
