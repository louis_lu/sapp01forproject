//
//  DBStoreInfoViewController.h
//  Dingbendong
//
//  Created by louis on 13/11/23.
//  Copyright (c) 2013年 louis.lu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBViewController.h"
#import "Store.h"

@interface DBStoreInfoViewController : UIViewController
<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray * stores;
    
}
@property (weak, nonatomic) IBOutlet UITableView *storeTableView;
@end
