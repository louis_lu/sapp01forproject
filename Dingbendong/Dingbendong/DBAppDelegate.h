//
//  DBAppDelegate.h
//  Dingbendong
//
//  Created by mac on 13/11/19.
//  Copyright (c) 2013年 louis.lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
