//
//  DBMenuViewController.h
//  Dingbendong
//
//  Created by louis on 13/11/23.
//  Copyright (c) 2013年 louis.lu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Store.h"
#import "Products.h"

@protocol DBMenuViewControlleDelegate <NSObject>
//@optional

@end



@interface DBMenuViewController : UIViewController<UINavigationControllerDelegate,UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray * products;
}
@property (weak, nonatomic) IBOutlet UITableView *menuTableView;
- (IBAction)nameTextField:(id)sender;
- (IBAction)phoneNumberTextField:(id)sender;
- (IBAction)addressTextField:(id)sender;
@property(nonatomic)Store *store;
@property(nonatomic,weak) id<DBMenuViewControlleDelegate> delegate;
@end
