//
//  DBCell.h
//  Dingbendong
//
//  Created by louis on 13/11/23.
//  Copyright (c) 2013年 louis.lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DBCell : UITableViewCell
///此class為客製化cell
@property (weak, nonatomic) IBOutlet UILabel *productNameTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *UnitPriceTextLabel;
@property (weak, nonatomic) IBOutlet UITextField *totalNumberTextField;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UIButton *reduceButton;

@end
