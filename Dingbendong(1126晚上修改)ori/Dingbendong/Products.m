//
//  Products.m
//  Dingbendong
//
//  Created by mac on 13/11/27.
//  Copyright (c) 2013年 louis.lu. All rights reserved.
//

#import "Products.h"

@implementation Products

@synthesize supplierID;
@synthesize productName;
@synthesize productID;
@synthesize unitPrice;


-(id)initWithCoder:(NSCoder *)aDecoder{
    //此時super是NSObject
    if(self=[super init]){
        self.supplierID=[aDecoder decodeObjectForKey:@"supplierID"];
        self.productName=[aDecoder decodeObjectForKey:@"productName"];
        self.productID=[aDecoder decodeObjectForKey:@"productID"];
        self.unitPrice=[aDecoder decodeObjectForKey:@"unitPrice"];
        
        
    }
    return self;
}
-(void)encodeWithCoder:(NSCoder *)aCoder{
    
    
    
    [aCoder encodeObject:self.supplierID forKey:@"supplierID"];
    [aCoder encodeObject:self.productName forKey:@"productName"];
    [aCoder encodeObject:self.productID forKey:@"productID"];
    [aCoder encodeObject:self.unitPrice forKey:@"unitPrice"];
}


@end
