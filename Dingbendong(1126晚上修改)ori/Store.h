//
//  Store.h
//  Dingbendong
//
//  Created by mac on 13/11/21.
//  Copyright (c) 2013年 louis.lu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Store : NSObject
{
    NSString * name;
    NSString * supplierID;
    NSString * address;
    NSString * phoneNumber;
}
@property(nonatomic, retain) NSString * name;
@property(nonatomic, retain) NSString * supplierID;
@property(nonatomic, retain) NSString * address;
@property(nonatomic, retain) NSString * phoneNumber;


@end
