//
//  DBStoreInfoDetailViewController.h
//  Dingbendong
//
//  Created by louis on 13/11/23.
//  Copyright (c) 2013年 louis.lu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBStoreInfoViewController.h"
#import "Store.h"

@protocol DBStoreInfoDetailViewControllerDelegate;

@protocol DBStoreInfoDetailViewControllerDelegate <NSObject>

@end

@interface DBStoreInfoDetailViewController : UIViewController

@property(nonatomic)Store *store;
@property (nonatomic,weak)id<DBStoreInfoDetailViewControllerDelegate>delegate;


@end
