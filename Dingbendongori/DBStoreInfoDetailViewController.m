//
//  DBStoreInfoDetailViewController.m
//  Dingbendong
//
//  Created by louis on 13/11/23.
//  Copyright (c) 2013年 louis.lu. All rights reserved.
//

#import "DBStoreInfoDetailViewController.h"


//#define localhost lulouis.no-ip.biz

@interface DBStoreInfoDetailViewController ()

@end

@implementation DBStoreInfoDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:self action:@selector(back:)];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
}

-(IBAction)back:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
