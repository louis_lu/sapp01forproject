//
//  DBMenuViewController.m
//  Dingbendong
//
//  Created by louis on 13/11/23.
//  Copyright (c) 2013年 louis.lu. All rights reserved.
//

#import "DBMenuViewController.h"
#import "DBViewController.h"
#import "DBCell.h"

//#define localhost lulouis.no-ip.biz

@interface DBMenuViewController ()

@end

@implementation DBMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.menuTableView.delegate=self;
    self.menuTableView.dataSource=self;
    self.navigationItem.title=self.store.name;
     products=[NSMutableArray array];
    
    //UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] init];
    //backButtonItem.title = @"Back";
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:self action:@selector(back:)];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
    self.navigationItem.rightBarButtonItems=@[self.buyBarButtonItem,self.helpBarButtonItem];
    


//    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
//                                                                                         target:self action:@selector(backBarButtonItem)  ];
	// Do any additional setup after loading the view.
  

        NSString *  storeWebsiteForProducts ;
        storeWebsiteForProducts =[[NSString alloc]initWithFormat:@"http://localhost:8888/sapp01/select_from_products_for_suppliersID.php?id=%@",self.store.supplierID];
        storeWebsiteForProducts = [storeWebsiteForProducts stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURLRequest *storeWebsiteForProductsrequest  =[NSURLRequest requestWithURL:[NSURL URLWithString:storeWebsiteForProducts]];
        
        NSData * storeWebsiteForProductsData =[NSURLConnection sendSynchronousRequest:storeWebsiteForProductsrequest returningResponse:nil error:nil];
        
        NSDictionary * theJSsonDict_storeWebsiteForProducts =
        [NSJSONSerialization JSONObjectWithData:storeWebsiteForProductsData options:NSJSONReadingAllowFragments error:nil];
        NSString * strTempstorelist;
        for( int i = 0 ; i < [theJSsonDict_storeWebsiteForProducts[@"array_of_rows"] count]; i++ )
        {
            strTempstorelist = [NSString stringWithFormat:@"%@\n", theJSsonDict_storeWebsiteForProducts[@"array_of_rows"][i][@"product_name"] ];
            NSLog(@"index%d %@",i, theJSsonDict_storeWebsiteForProducts[@"array_of_rows"][i][@"product_name"]);
        }
        
        
        
        for(int i=0;i<[theJSsonDict_storeWebsiteForProducts[@"array_of_rows"] count];i++){
            
            Products *product =[[Products alloc]init];
            NSString * pName =[NSString stringWithFormat:theJSsonDict_storeWebsiteForProducts[@"array_of_rows"][i][@"product_name"],i];
            NSString * pID =[NSString stringWithFormat:theJSsonDict_storeWebsiteForProducts[@"array_of_rows"][i][@"product_id"],i];
            NSString * uPrice =[NSString stringWithFormat:theJSsonDict_storeWebsiteForProducts[@"array_of_rows"][i][@"unit_price"],i];
            NSString * sID=[NSString stringWithFormat:theJSsonDict_storeWebsiteForProducts[@"array_of_rows"][i][@"supplier_id"],i];
            
            product.productName=pName;
            product.productID=pID;
            product.unitPrice=uPrice;
            product.supplierID=sID;
            
            [products addObject:product];
            
        }
    UITapGestureRecognizer*tap=[[UITapGestureRecognizer alloc]init];
    tap.cancelsTouchesInView = NO;//預設值是YES
    [tap addTarget:self action:@selector(dissmissKeyboard)];
    [self.view addGestureRecognizer:tap];

    
}

-(IBAction)back:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}



-(void)dissmissKeyboard{
    
    if ([self.nameTextField isFirstResponder]) {
        [self.nameTextField resignFirstResponder];
    }
    if ([self.phoneNumberTextField isFirstResponder]) {
        [self.phoneNumberTextField resignFirstResponder];
    }
    if ([self.addressTextField isFirstResponder]) {
        [self.addressTextField resignFirstResponder];
    }

    
}

#pragma mark UITableView protocol
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return products.count ;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Products * product=products[indexPath.row];
    
    
    
     //custom cell
     DBCell *mycell=[tableView dequeueReusableCellWithIdentifier:@"customcell" forIndexPath:indexPath];
    mycell.productNameTextLabel.text=product.productName;
    mycell.UnitPriceTextLabel.text=[product.unitPrice stringByAppendingString:@"$"];
    
 
    
    return mycell;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
