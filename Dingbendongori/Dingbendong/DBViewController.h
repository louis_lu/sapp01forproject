//
//  DBViewController.h
//  Dingbendong
//
//  Created by mac on 13/11/19.
//  Copyright (c) 2013年 louis.lu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Store.h"
#import "Products.h"

@interface DBViewController : UIViewController
<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray * stores;
    int m_nCurrentSelect;
}
@property (weak, nonatomic) IBOutlet UITableView *storeTableView;
//@property(nonatomic)Store *store;


@end
