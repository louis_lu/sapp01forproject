//
//  DBCell.m
//  Dingbendong
//
//  Created by louis on 13/11/23.
//  Copyright (c) 2013年 louis.lu. All rights reserved.
//

#import "DBCell.h"

@implementation DBCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)valueChanged:(UIStepper*)sender {
    
    UIStepper *stepper = (UIStepper *) sender;
    
    stepper.maximumValue = 100;
    if(stepper.minimumValue == 0){
        stepper.minimumValue = 0;
    }else{
        stepper.minimumValue =[self.totalNumberTextField.text intValue];
    }
    
    double value = [sender value];
    
    [self.totalNumberTextField setText:[NSString stringWithFormat:@"%d", (int)value]];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([self.totalNumberTextField isFirstResponder]) {
        [self.totalNumberTextField resignFirstResponder];
    }
    return YES;
}



@end
