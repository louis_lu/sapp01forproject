//
//  main.m
//  Dingbendong
//
//  Created by mac on 13/11/19.
//  Copyright (c) 2013年 louis.lu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DBAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DBAppDelegate class]));
    }
}
