//
//  DBMenuViewController.h
//  Dingbendong
//
//  Created by louis on 13/11/23.
//  Copyright (c) 2013年 louis.lu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Store.h"
#import "Products.h"


@protocol DBMenuViewControlleDelegate;
@protocol DBMenuViewControlleDelegate <NSObject>
//@optional

@end



@interface DBMenuViewController : UIViewController<UINavigationControllerDelegate,UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray * products;
}
@property (weak, nonatomic) IBOutlet UITableView *menuTableView;

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (nonatomic,weak) IBOutlet UIBarButtonItem *helpBarButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *buyBarButtonItem;

@property(nonatomic)Store *store;
@property(nonatomic,weak) id<DBMenuViewControlleDelegate> delegate;
@end
