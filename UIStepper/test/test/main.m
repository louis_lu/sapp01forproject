//
//  main.m
//  test
//
//  Created by mac on 13/11/28.
//  Copyright (c) 2013年 louis.lu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
