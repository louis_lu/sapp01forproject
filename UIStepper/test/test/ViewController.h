//
//  ViewController.h
//  test
//
//  Created by mac on 13/11/28.
//  Copyright (c) 2013年 louis.lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIStepper *step;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@end
